<!doctype html>
<html lang="en">

    <head>
        
        <meta charset="utf-8" />
        <title>TURN | Admin</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- App favicon -->
        <link rel="shortcut icon" href="<?=base_url()?>/assets/images/favicon.ico">

        <!-- DataTables -->
        <link href="<?=base_url()?>/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- Responsive datatable examples -->
        <link href="<?=base_url()?>/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- preloader css -->
        <link rel="stylesheet" href="<?=base_url()?>/assets/css/preloader.min.css" type="text/css" />

        <!-- Bootstrap Css -->
        <link href="<?=base_url()?>/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="<?=base_url()?>/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="<?=base_url()?>/assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
        <style>
            .help-block{
                color: red !important;
            }
            .color-red{
                color: red !important;
            }
        </style>
    </head>

    <body>

    <!-- <body data-layout="horizontal"> -->

        <!-- Begin page -->
        <div id="layout-wrapper">

            

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                    <h4 class="mb-sm-0 font-size-18">Lista de Administración de Vehículos TURN</h4>
                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

                        <div class="row align-items-center">
                            <div class="col-md-6">
                            </div>

                            <div class="col-md-6">
                                <div class="d-flex flex-wrap align-items-center justify-content-end gap-2 mb-3">
                                    <div>
                                        <a href="#" class="btn btn-light" data-bs-toggle="modal" data-bs-target="#exampleModalFullscreen"><i class="bx bx-plus me-1"></i> Agregar Nuevo</a>
                                    </div>
                                    <div>
                                        <a href="javascript:void(0);" class="btn btn-primary close-session"> Cerrar Sesión</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- end row -->

                        <div class="table-responsive mb-4">
                            <table class="table align-middle datatable dt-responsive table-check nowrap" style="border-collapse: collapse; border-spacing: 0 8px; width: 100%;">
                                <thead>
                                  <tr>
                                    <th scope="col">Automovil</th>
                                    <th scope="col" class="marca-table">Marca</th>
                                    <th scope="col" class="modelo-table">Modelo</th>
                                    <th scope="col">Precio</th>
                                    <th scope="col">Estatus</th>
                                    <th style="width: 80px; min-width: 80px;">Action</th>
                                  </tr>
                                </thead>
                                <tbody id="vehicles-table">
                                </tbody>
                            </table>
                            <!-- end table -->
                        </div>
                        <!-- end table responsive -->

                        <!-- sample modal content -->
                        <div id="exampleModalFullscreen" class="modal fade" tabindex="-1" aria-labelledby="exampleModalFullscreenLabel" aria-hidden="true">
                            <div class="modal-dialog modal-fullscreen">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalFullscreenLabel">Información del vehículo</h5>
                                        <button id="close-modal-form-nodatatable" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <form id="form-car" novalidate action="" method="POST">
                                            <input type="hidden" id="id" name="id" class="id-car" value="">
                                        
                                            <div class="container" style="max-width: 960px !important; margin: 30px auto !important; padding: 20px !important;margin-top: -65px !important; margin-bottom: -40px !important;">
                                                <div class="avatar-upload" style="position: relative !important; max-width: 205px !important; margin: 50px auto !important;">
                                                    <div class="avatar-edit">
                                                        <input type='file' name="myfile" id="myfile" accept=".png, .jpg, .jpeg" />
                                                        <label id="label-image" for="myfile"></label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xl-12 col-md-12">
                                                    <div class="form-group mb-3">
                                                        <label>Nombre <span class="color-red">*</span></label>
                                                        <input id="nombre" name="nombre" type="text" class="form-control nombre-car" value="" />
                                                    </div>
                                                </div>
                                                
                                                <div class="col-xl-12 col-md-12">
                                                    <div class="form-group mb-3">
                                                        <label>Precio <span class="color-red">*</span></label>
                                                        <input id="precio" name="precio" type="number" class="form-control precio-car" value="" />
                                                    </div>
                                                </div>

                                                <div class="col-xl-12 col-md-12">
                                                    <div class="form-group mb-3">
                                                        <label>Marca <span class="color-red">*</span></label>
                                                        <input id="marca" name="marca" type="text" class="form-control marca-car" value="" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-12 col-md-12">
                                                    <div class="form-group mb-3">
                                                        <label>Modelo <span class="color-red">*</span></label>
                                                        <input id="modelo" name="modelo" type="text" class="form-control modelo-car" value="" />
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- end row -->
                                            <div class="form-group" style="display: none;">
                                                <button type="submit" class="btn btn-primary float-right save-form">Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button id="close-modal-form-nodatatable" type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Cerrar</button>
                                        <button id="save-form" type="button" class="btn btn-primary waves-effect waves-light">Guardar</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

                <div id="myModal" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="myModalLabel">Información detallada</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body" id="datos-vehiculo">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Cerrar</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>

                
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <script>document.write(new Date().getFullYear())</script> © TURN.
                            </div>
                            <div class="col-sm-6">
                                <div class="text-sm-end d-none d-sm-block">
                                    Design & Develop by <a href="#!" class="text-decoration-underline">Yisus</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- JAVASCRIPT -->
        <script src="<?=base_url()?>/assets/libs/jquery/jquery.min.js"></script>
        <script src="<?=base_url()?>/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?=base_url()?>/assets/libs/metismenu/metisMenu.min.js"></script>
        <script src="<?=base_url()?>/assets/libs/simplebar/simplebar.min.js"></script>
        <script src="<?=base_url()?>/assets/libs/node-waves/waves.min.js"></script>
        <script src="<?=base_url()?>/assets/libs/feather-icons/feather.min.js"></script>
        <script src="<?=base_url()?>/assets/js/jquery.validate.js"></script>

        <script>
		 var base = '<?= base_url(); ?>';
		</script>

        <script src="<?=base_url()?>/assets/js/modules/cars.funtions.js"></script>

        <!-- pace js -->
        <script src="<?=base_url()?>/assets/libs/pace-js/pace.min.js"></script>

        <script src="<?=base_url()?>/assets/js/app.js"></script>

    </body>
</html>
