<!doctype html>
<html lang="en">

    <head>
        
        <meta charset="utf-8" />
        <title>Turn Vehículos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- App favicon -->
        <link rel="shortcut icon" href="<?=base_url()?>/assets/images/favicon.ico">
        <!-- C:\xampp\htdocs\turn\public\assets\images\favicon.ico -->
        <!-- preloader css -->
        <link rel="stylesheet" href="<?=base_url()?>/assets/css/preloader.min.css" type="text/css" />
        
        <!-- Bootstrap Css -->
        <link href="<?=base_url()?>/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="<?=base_url()?>/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="<?=base_url()?>/assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />

    </head>

    <body>

    <!-- <body data-layout="horizontal"> -->

        <!-- Begin page -->
        <div id="layout-wrapper">
            

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content" style="margin-left: 0px !important;">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                    <h4 class="mb-sm-0 font-size-18">Venta de Vehículos TURN</h4>
                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

                        <div class="row align-items-center">
                            <div class="col-md-6">
                            </div>

                            <div class="col-md-6">
                                <div class="d-flex flex-wrap align-items-center justify-content-end gap-2 mb-3">
                                    <div>
                                        <a href="<?=base_url()?>" class="btn btn-primary"><i class="bx bx-user me-1"></i> Regresar al Inicio</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- end row -->
                        <div class="card-body">

                            <h4 class="font-size-16">¿Como puedo convertirme en administrador?</h4>

                            <p>Solo basta con consultar nuesta lista de vehiculos.</p>
                            <p>Tiene que hacer una petición a través de nuestra API http://localhost:3001/api/tracks.</p>
                            <p>La petición deber ser un request HTTP con el método GET, al hacer esto de forma correcta puede obtener la lista de automóviles para mostrar en su sitio.</p>
                            <p>Le recomendamos identificar el proveedor para el contacto en caso de hacer la venta de un vehículo.</p>
                            <p>Se recomienda utilizar la herramienta Postman para hacer pruebas de integración https://www.postman.com/.</p>
                            <hr/>

                            <a href="<?=base_url()?>" class="btn btn-secondary waves-effect mt-4"><i class="mdi mdi-reply me-1"></i> Volver</a>
                        </div>
                        <!-- end row -->
                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

        </div>
        <!-- END layout-wrapper -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- JAVASCRIPT -->
        <script src="<?=base_url()?>/assets/libs/jquery/jquery.min.js"></script>
        <script src="<?=base_url()?>/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?=base_url()?>/assets/libs/metismenu/metisMenu.min.js"></script>
        <script src="<?=base_url()?>/assets/libs/simplebar/simplebar.min.js"></script>
        <script src="<?=base_url()?>/assets/libs/node-waves/waves.min.js"></script>
        <script src="<?=base_url()?>/assets/libs/feather-icons/feather.min.js"></script>

        <!-- pace js -->
        <script src="<?=base_url()?>/assets/libs/pace-js/pace.min.js"></script>

        <script src="<?=base_url()?>/assets/js/app.js"></script>

    </body>
</html>
