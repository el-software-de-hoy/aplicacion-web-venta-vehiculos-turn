<!doctype html>
<html lang="en">

    <head>
        
        <meta charset="utf-8" />
        <title>Turn Vehículos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- App favicon -->
        <link rel="shortcut icon" href="<?=base_url()?>/assets/images/favicon.ico">
        <!-- C:\xampp\htdocs\turn\public\assets\images\favicon.ico -->
        <!-- preloader css -->
        <link rel="stylesheet" href="<?=base_url()?>/assets/css/preloader.min.css" type="text/css" />
        
        <!-- Bootstrap Css -->
        <link href="<?=base_url()?>/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="<?=base_url()?>/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="<?=base_url()?>/assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />

    </head>

    <body>

    <!-- <body data-layout="horizontal"> -->

        <!-- Begin page -->
        <div id="layout-wrapper">
            

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content" style="margin-left: 0px !important;">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                    <h4 class="mb-sm-0 font-size-18">Venta de Vehículos TURN</h4>
                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

                        <div class="row align-items-center">
                            <div class="col-md-6">
                            </div>

                            <div class="col-md-6">
                                <div class="d-flex flex-wrap align-items-center justify-content-end gap-2 mb-3">
                                    <div>
                                        <a href="javascript:void(0);" class="btn btn-primary open-filter"> Buscar</a>
                                    </div>
                                    <div>
                                        <a href="javascript:void(0);" class="btn btn-primary mas-menos"> Ordenar Precio + a -</a>
                                    </div>
                                    <div>
                                        <a href="javascript:void(0);" class="btn btn-primary menos-mas"> Ordenar Precio - a +</a>
                                    </div>
                                    <div>
                                        <a href="<?=base_url()?>/distribuidor" class="btn btn-light"><i class="bx bx-user me-1"></i> Quiero ser distribuidor</a>
                                    </div>
                                    <div>
                                        <a href="<?=base_url()?>/login" class="btn btn-light"><i class="bx bx-user me-1"></i> Soy Admin</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- end row -->

                        <div id="filters" class="row" style="display: none;">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <i id="close-filter" class="bx bx-x font-size-20 cursor-pointer" style="float: right; font-size: 20.px; cursor: pointer;"></i>
                                        <h3 class="card-title">Buscar</h3>
                                        <p class="card-text">Encuentra rapidamente el vehículo que buscas.</p>
                                        <form id="form-filters" action="">
                                            <div class="row">
                                                <div class="col-xl-4 col-md-12">
                                                    <div class="form-group mb-3">
                                                        <label>Nombre</label>
                                                        <input id="nombre" name="nombre" type="text" class="form-control nombre-filter" value="" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-4 col-md-12">
                                                    <div class="form-group mb-3">
                                                        <label>Marca</label>
                                                        <input id="marca" name="marca" type="text" class="form-control marca-filter" value="" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-4 col-md-12">
                                                    <div class="form-group mb-3">
                                                        <label>Modelo</label>
                                                        <input id="modelo" name="modelo" type="text" class="form-control modelo-filter" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <a href="javascript: void(0);" class="btn btn-primary waves-effect waves-light w-18 search-filters-nodatatable" style="float: right;">Buscar</a>
                                        <a href="javascript: void(0);" class="btn btn-primary waves-effect waves-light w-18 margin-right clean-filters-nodatatable" style="float: right;">Limpiar</a>
                                    </div>
                                </div>
                            </div><!-- end col -->
                        </div>

                        <div id="vehicles-table" class="row">
                        </div>
                        <!-- end row -->
                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

        </div>
        <!-- END layout-wrapper -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- JAVASCRIPT -->
        <script src="<?=base_url()?>/assets/libs/jquery/jquery.min.js"></script>
        <script src="<?=base_url()?>/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?=base_url()?>/assets/libs/metismenu/metisMenu.min.js"></script>
        <script src="<?=base_url()?>/assets/libs/simplebar/simplebar.min.js"></script>
        <script src="<?=base_url()?>/assets/libs/node-waves/waves.min.js"></script>
        <script src="<?=base_url()?>/assets/libs/feather-icons/feather.min.js"></script>

        <script src="<?=base_url()?>/assets/js/modules/home.functions.js"></script>

        <!-- pace js -->
        <script src="<?=base_url()?>/assets/libs/pace-js/pace.min.js"></script>

        <script src="<?=base_url()?>/assets/js/app.js"></script>

    </body>
</html>
