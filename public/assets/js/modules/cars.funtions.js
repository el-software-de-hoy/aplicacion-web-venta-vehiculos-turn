$( document ).ready(function() {

    $(function(){
        var token = localStorage.getItem("token");
        if(token == null || token == "" || token == undefined){
            window.location.href = base+"/login";
        }
        getTable();
        formValidate();
    });

    $( "body" ).on('click', '#close-modal-form-nodatatable',function() {
        $("#form-car")[0].reset();
        $('.form-group').removeClass('has-error');
        var validator = $( "#form-car" ).validate();
        validator.resetForm();
    });

    $( "body" ).on('click', '#save-form',function() {
        $('.save-form').trigger('click');
    });

    $( "body" ).on('click', '.close-session',function() {
         localStorage.removeItem("token");
         localStorage.removeItem("email");
         localStorage.removeItem("name");
         localStorage.removeItem("role");
         localStorage.removeItem("type");
         localStorage.removeItem("id");
         window.location.href = base+"/login";
    });

    

    $( "body" ).on('click', '.delete',function() {
        var id = $(this).data('id');
        var token = localStorage.getItem("token");
        var role = localStorage.getItem("role");
        if(role == "admin"){
            var settings = {
                "url": "http://localhost:3001/api/tracks/"+id,
                "method": "DELETE",
                "timeout": 0,
                "headers": {
                  "Authorization": 'Bearer token '+token
                },
              };
              
              $.ajax(settings).done(function (response) {
                if(response['success'] == 1){
                    alert("Se elimino correctamente");
                    getTable();
                }else{
                    alert(response['message']);
                    if(response['message'] == "La sesion a expirado"){
                        localStorage.removeItem("token");
                        window.location.href = base+"/login";
                    }
                }
              });
        }else{
            alert("El usuario no tiene permisos para esta acción")
        }

    });

    $( "body" ).on('click', '.update',function() {
        var role = localStorage.getItem("role");
        if(role == "admin"){

            var id = $(this).data('id');
            var status = $(this).data('status');
            var token = localStorage.getItem("token");
            if(status == 0){
                var settings = {
                    "url": 'http://localhost:3001/api/tracks/status/'+id,
                    "method": "PUT",
                    "timeout": 0,
                    "headers": {
                    "Authorization": 'Bearer token '+token
                    },
                };
                
                $.ajax(settings).done(function (response) {
                    if(response.success == 1){
                        alert("Estatus Modificado");
                        getTable();
                    }else{
                        alert(response['message']);
                        if(response['message'] == "La sesion a expirado"){
                            localStorage.removeItem("token");
                            window.location.href = base+"/login";
                        }
                    }
                });
            }else{
                var settings = {
                    "url": 'http://localhost:3001/api/tracks/'+id,
                    "method": "PUT",
                    "timeout": 0,
                    "headers": {
                    "Authorization": 'Bearer token '+token
                    },
                };
                
                $.ajax(settings).done(function (response) {
                    if(response.success == 1){
                        alert("Estatus Modificado");
                        getTable();
                    }else{
                        alert(response['message']);
                        if(response['message'] == "La sesion a expirado"){
                            localStorage.removeItem("token");
                            window.location.href = base+"/login";
                        }
                    }
                });
            }

        }else{
            alert("El usuario no tiene permisos para esta acción")  
        }
    });
    
    $( "body" ).on('click', '.show-detail',function() {
        var id = $(this).data('id');
        $('#datos-vehiculo').html("");
        var settings = {
            "url": 'http://localhost:3001/api/tracks/'+id,
            "method": "GET",
            "timeout": 0,
          };
          
          $.ajax(settings).done(function (response) {
            $('#myModal').modal('toggle');

            var role = localStorage.getItem("role");
            var type = localStorage.getItem("type");

            var html = `<div>
                            <p>Nombre: `+response.data[0]['nombre']+`</p>
                            <p>Precio: `+response.data[0]['precio']+`</p>`;
                            if(role == "admin" && type == 0){

                            }else{
                                html += `<p>Marca: `+response.data[0]['marca']+`</p>
                                <p>Modelo:`+response.data[0]['modelo']+` </p>`;
                            }

                            if(response.data[0]['status'] == 1){
                                html += `<p>Estatus: Disponible</p>`;
                            }else{
                                html += `<p>Estatus: Vendido</p>`;
                            }
                            html += `<img src="`+response.data[0]['image']['url']+`" alt="auto" style="width: 100%;">
                        </div>`;

            $('#datos-vehiculo').append(
                html
            ); 

          });
    });

});

function getTable(){

    $('#vehicles-table').html("");
    $.get('http://localhost:3001/api/tracks',function(data) {
        if (data.data.length > 0) { 
        
            var role = localStorage.getItem("role");
            var type = localStorage.getItem("type");

            if(role == "admin" && type == 0){
                $('.marca-table').hide();
                $('.modelo-table').hide();
            }

            $.each(data.data, function(index, value) {
                
                if(value !== null && value !== undefined){
                                    
                                    html = `<tr>
                                    <td>
                                        <img src="`+value['image']['url']+`" alt="" class="avatar-sm rounded-circle me-2">
                                        <a href="Javascript:void(0)" class="text-body">`+value['nombre']+`</a>
                                    </td>`;

                                    if(role == "admin" && type == 0){
                                        
                                    }else{
                                        html += `<td>`+value['marca']+`</td>
                                        <td>`+value['modelo']+`</td>`;
                                    }

                                    html += `<td>`+value['precio']+`</td>`;
                                    if(value['status'] == 1){
                                        html += `<td>Disponible</td>`;
                                    }else{
                                        html += `<td>Vendido</td>`;
                                    }
                                    
                                    html += `<td>
                                        <div class="dropdown">
                                            <button class="btn btn-link font-size-16 shadow-none py-0 text-muted dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                <i class="bx bx-dots-horizontal-rounded"></i>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-end">
                                                <li><a class="dropdown-item show-detail" href="Javascript:void(0)" data-id="`+value['_id']+`">Ver</a></li>
                                                <li><a class="dropdown-item update" href="Javascript:void(0)" data-id="`+value['_id']+`" data-status="`+value['status']+`">Cambiar estatus</a></li>
                                                <li><a class="dropdown-item delete" href="Javascript:void(0)" data-id="`+value['_id']+`">Eliminar</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>`;

                        $('#vehicles-table').append(
                            html
                        ); 
                    
                }
                
            }); 
        }else{

           $('#vehicles-table').append(`<div class="row">
           <div class="col-lg-12">
               <div class="card">
                   <div class="card-body">

                       <div class="row mt-5">
                           <div>
                               <div class="card">
                                   <div class="card-body overflow-hidden position-relative">
                                       <div>
                                           <i class="bx bx-help-circle widget-box-1-icon text-primary"></i>
                                       </div>
                                           <div class="faq-count">
                                           <h5 class="text-primary">:)</h5>
                                           </div>
                                       <h5 class="mt-3 aling-center">¿No puedes ver vehículos registrado?</h5>
                                       <p class="text-muted mt-3 mb-0 aling-center">Descuida esto no es un error.</p>
                                       <p class="text-muted mt-3 mb-0 aling-center">Pronto los podrás ver.</p>
                                   </div>
                                   <!-- end card body -->
                               </div>
                               <!-- end card -->
                           </div>
                           <!-- end col -->
                       </div>
                       <!-- end row -->
                   </div>
                   <!-- end  card body -->
               </div>
               <!-- end card -->
           </div>
           <!-- end col -->
       </div>
       <!-- end row -->`
); 
        }
        
    });
}

var formValidate = function(){
    $('#form-car').validate({
        rules: {
            nombre: {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
            },
            precio: {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
            },
            imageUpload: {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
            },
            marca: {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
            },
            modelo: {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
            },
        },
        messages: {
            nombre: {
                required:  'El nombre es obligatorio.'
            },
            precio: {
                required: 'El precio es obligatorio.'
            },
            imageUpload: {
                required: 'La imagen es obligatoria.'
            },
            marca: {
                required: 'La imagen es obligatoria.'
            },
            modelo: {
                required: 'La imagen es obligatoria.'
            },
        },
        
        highlight: function(element) {
            $(element).closest('.form-control').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-control').removeClass('has-error');
        },
        invalidHandler: function(form, validator) {
            if (!validator.numberOfInvalids())
                return;
        },
        errorElement: 'form-control',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            var role = localStorage.getItem("role");
            if(role == "admin"){

                var filePath=$('#myfile').val(); 
                var nombre = $('#nombre').val();   
                var precio = $('#precio').val(); 
                var marca = $('#marca').val(); 
                var modelo = $('#modelo').val(); 

                
                if(filePath == ""){
                    alert("La imagen del vehículo es obligatoria carge el archivo");
                }else{

                    var form = new FormData();
                    form.append("myfile", myfile.files[0], filePath);

                    var settings = {
                    "url": "http://localhost:3001/api/storage",
                    "method": "POST",
                    "timeout": 0,
                    "processData": false,
                    "mimeType": "multipart/form-data",
                    "contentType": false,
                    "data": form
                    };

                    $.ajax(settings).done(function (response) {
                        var json = JSON.parse(response);

                        if(json.success == 1){
                            var datos = json.datos;
                            var idImage = datos['_id'];
                            var token = localStorage.getItem("token");

                            var settings2 = {
                                "url": "http://localhost:3001/api/tracks",
                                "method": "POST",
                                "timeout": 0,
                                "headers": {
                                "Authorization": "Bearer token "+token,
                                "Content-Type": "application/json"
                                },
                                "data": JSON.stringify({
                                "nombre": nombre,
                                "precio": precio,
                                "modelo": modelo,
                                "marca": marca,
                                "mediaId": idImage
                                }),
                            };
                            
                            $.ajax(settings2).done(function (response) {
                                if(response.success == 1){
                                    $("#form-car")[0].reset();
                                    $('.form-group').removeClass('has-error');
                                    var validator = $( "#form-car" ).validate();
                                    validator.resetForm();
                                    $('#close-modal-form-nodatatable').trigger('click');
                                    getTable();

                                }else{
                                    alert(response['message']);
                                    if(response['message'] == "La sesion a expirado"){
                                        localStorage.removeItem("token");
                                        window.location.href = base+"/login";
                                    }
                                }
                            });
                        }else{
                            alert("No se pudo cargar la imagen intente de nuevo");
                        }
                    });

                }
            }else{
                alert("El usuario no tiene permisos para esta acción")
            }
        }
    });

};