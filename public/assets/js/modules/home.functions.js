$( document ).ready(function() {
    $(function(){
        getTable();
    });


    
    $( "body" ).on('click', '.open-filter',function() {
        $('#filters').show();
    });

    $( "body" ).on('click', '#close-filter',function() {
        $('#filters').hide();
    });

    $( "body" ).on('click', '.clean-filters-nodatatable',function() {
        $('#nombre').val("");
        $('#marca').val("");
        $('#modelo').val("");
        getTable();
    });

    $( "body" ).on('click', '.search-filters-nodatatable',function() {
        var nombre = $('#nombre').val();
        var marca = $('#marca').val();
        var modelo = $('#modelo').val();
        var settings = {
            "url": "http://localhost:3001/api/tracks/search",
            "method": "POST",
            "timeout": 0,
            "headers": {
              "Content-Type": "application/json"
            },
            "data": JSON.stringify({
              "nombre": nombre,
              "marca": marca,
              "modelo": modelo
            }),
          };
          
          $.ajax(settings).done(function (response) {
            
            if(response.data.length > 0){
                $('#vehicles-table').html("");
                $.each(response.data, function(index, value) {
                    
                    $.get('http://localhost:3001/api/tracks/'+value['_id'] ,function(data) {
                        if (data.data.length > 0) { 
                            html=`<div class="col-xl-3 col-sm-6">
                                    <div class="card text-center">
                                        <div class="card-body">
                                            
                                            <div class="mx-auto mb-4">
                                                <img src="`+data.data[0]['image']['url']+`" alt="" class="avatar-xl rounded-circle img-thumbnail">
                                            </div>
                                            <h5 class="font-size-16 mb-1"><a href="#" class="text-dark">`+data.data[0]['nombre']+`</a></h5>
                                            <p class="text-muted mb-2">`+data.data[0]['marca']+`</p>
                                            <p class="text-muted mb-2">`+data.data[0]['modelo']+`</p>
                                            <p class="text-muted mb-2">`+data.data[0]['precio']+`</p>`;
                                            if(data.data[0]['status'] == 1){
                                                html +=`<p class="text-muted mb-2">Disponible</p>`;
                                            }else{
                                                html +=`<p class="text-muted mb-2">Vendido</p>`;
                                            }
                                        html += `</div>
    
                                        <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-outline-light text-truncate dollar-price" data-id="`+data.data[0]['precio']+`"><a href="Javascript:void(0)"><i class="uil uil-user me-1"></i> Ver precio en dolares</a></button>
                                        </div>
                                    </div>
                                    <!-- end card -->
                                </div>`;

                        $('#vehicles-table').append(
                            html
                        ); 
                        }
                    });
                }); 
            }else{
                alert("No se encontraron resultados");
            }
          });

    });

    $( "body" ).on('click', '.mas-menos',function() {
        getTableMas();
    });

    $( "body" ).on('click', '.menos-mas',function() {
        getTableMenos();
    });

    $( "body" ).on('click', '.dollar-price',function() {
        var precio = $(this).data('id');
        
        let date = new Date()

        let day = date.getDate()
        let dayAnt = date.getDate() - 1;
        let month = date.getMonth() + 1
        let year = date.getFullYear()

        if(month < 10){
            var fechaFin = `${year}-0${month}-${day}`;
            var fechaIni = `${year}-0${month}-${dayAnt}`;
        }else{
            var fechaFin = `${year}-${month}-${day}`;
            var fechaIni = `${year}-${month}-${dayAnt}`;
        }

        var settings = {
            "url": 'https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43718/datos/'+fechaIni+'/'+fechaFin+'?token=20556ab83173c3391b50e33c48f394dfb123a6d875be14e6a694fcad568ad69b',
            "method": "GET",
            "timeout": 0,
          };
          
          $.ajax(settings).done(function (response) {
            var tipoCambio = response.bmx.series[0]['datos'][0]['dato'];
            var dolares = precio / tipoCambio;
            alert("El precio del vehículo en dolares es: "+dolares.toFixed(2));
          });
    });

    

});

function getTable(){

    $('#vehicles-table').html("");
    $.get('http://localhost:3001/api/tracks',function(data) {
        if (data.data.length > 0) { 
        

            $.each(data.data, function(index, value) {
                
                if(value !== null && value !== undefined){
                    if(value['status'] == 1){

                                    html=`<div class="col-xl-3 col-sm-6">
                                    <div class="card text-center">
                                        <div class="card-body">
                                            
                                            <div class="mx-auto mb-4">
                                                <img src="`+value['image']['url']+`" alt="" class="avatar-xl rounded-circle img-thumbnail">
                                            </div>
                                            <h5 class="font-size-16 mb-1"><a href="#" class="text-dark">`+value['nombre']+`</a></h5>
                                            <p class="text-muted mb-2">`+value['marca']+`</p>
                                            <p class="text-muted mb-2">`+value['modelo']+`</p>
                                            <p class="text-muted mb-2">`+value['precio']+`</p>`;
                                            if(value['status'] == 1){
                                                html +=`<p class="text-muted mb-2">Disponible</p>`;
                                            }else{
                                                html +=`<p class="text-muted mb-2">Vendido</p>`;
                                            }
                                        html += `</div>
    
                                        <div class="btn-group" role="group">
                                            <button type="button" class="btn btn-outline-light text-truncate dollar-price" data-id="`+value['precio']+`"><a href="Javascript:void(0)"><i class="uil uil-user me-1"></i> Ver precio en dolares</a></button>
                                        </div>
                                    </div>
                                    <!-- end card -->
                                </div>`;

                        $('#vehicles-table').append(
                            html
                        ); 
                    }
                }
                
            }); 
        }else{

           $('#vehicles-table').append(`<div class="row">
           <div class="col-lg-12">
               <div class="card">
                   <div class="card-body">

                       <div class="row mt-5">
                           <div>
                               <div class="card">
                                   <div class="card-body overflow-hidden position-relative">
                                       <div>
                                           <i class="bx bx-help-circle widget-box-1-icon text-primary"></i>
                                       </div>
                                           <div class="faq-count">
                                           <h5 class="text-primary">:)</h5>
                                           </div>
                                       <h5 class="mt-3 aling-center">¿No puedes ver vehículos registrado?</h5>
                                       <p class="text-muted mt-3 mb-0 aling-center">Descuida esto no es un error.</p>
                                       <p class="text-muted mt-3 mb-0 aling-center">Pronto los podrás ver.</p>
                                   </div>
                                   <!-- end card body -->
                               </div>
                               <!-- end card -->
                           </div>
                           <!-- end col -->
                       </div>
                       <!-- end row -->
                   </div>
                   <!-- end  card body -->
               </div>
               <!-- end card -->
           </div>
           <!-- end col -->
       </div>
       <!-- end row -->`
); 
        }
        
    });
}

function getTableMenos(){
    //var company_id = $(".company-id-filter").val();

    $('#vehicles-table').html("");
    $.get('http://localhost:3001/api/tracks',function(data) {
        if (data.data.length > 0) { 
            

            var orden = data.data.sort(((a, b) => a.precio - b.precio));
        

            $.each(data.data, function(index, value) {
                
                if(value !== null && value !== undefined){
                    if(value['status'] == 1){

                                    html=`<div class="col-xl-3 col-sm-6">
                                    <div class="card text-center">
                                        <div class="card-body">
                                            
                                            <div class="mx-auto mb-4">
                                                <img src="`+value['image']['url']+`" alt="" class="avatar-xl rounded-circle img-thumbnail">
                                            </div>
                                            <h5 class="font-size-16 mb-1"><a href="#" class="text-dark">`+value['nombre']+`</a></h5>
                                            <p class="text-muted mb-2">`+value['marca']+`</p>
                                            <p class="text-muted mb-2">`+value['modelo']+`</p>
                                            <p class="text-muted mb-2">`+value['precio']+`</p>`;
                                            if(value['status'] == 1){
                                                html +=`<p class="text-muted mb-2">Disponible</p>`;
                                            }else{
                                                html +=`<p class="text-muted mb-2">Vendido</p>`;
                                            }
                                        html += `</div>
    
                                        <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-outline-light text-truncate dollar-price" data-id="`+value['precio']+`"><a href="Javascript:void(0)"><i class="uil uil-user me-1"></i> Ver precio en dolares</a></button>
                                        </div>
                                    </div>
                                    <!-- end card -->
                                </div>`;

                        $('#vehicles-table').append(
                            html
                        ); 
                    }
                }
                
            }); 
        }else{

           $('#vehicles-table').append(`<div class="row">
           <div class="col-lg-12">
               <div class="card">
                   <div class="card-body">

                       <div class="row mt-5">
                           <div>
                               <div class="card">
                                   <div class="card-body overflow-hidden position-relative">
                                       <div>
                                           <i class="bx bx-help-circle widget-box-1-icon text-primary"></i>
                                       </div>
                                           <div class="faq-count">
                                           <h5 class="text-primary">:)</h5>
                                           </div>
                                       <h5 class="mt-3 aling-center">¿No puedes ver vehículos registrado?</h5>
                                       <p class="text-muted mt-3 mb-0 aling-center">Descuida esto no es un error.</p>
                                       <p class="text-muted mt-3 mb-0 aling-center">Pronto los podrás ver.</p>
                                   </div>
                                   <!-- end card body -->
                               </div>
                               <!-- end card -->
                           </div>
                           <!-- end col -->
                       </div>
                       <!-- end row -->
                   </div>
                   <!-- end  card body -->
               </div>
               <!-- end card -->
           </div>
           <!-- end col -->
       </div>
       <!-- end row -->`
); 
        }
        
    });
}

function getTableMas(){
    //var company_id = $(".company-id-filter").val();

    $('#vehicles-table').html("");
    $.get('http://localhost:3001/api/tracks',function(data) {
        if (data.data.length > 0) { 
            
            var orden = data.data.sort(((a, b) => b.precio - a.precio));
        

            $.each(data.data, function(index, value) {
                
                if(value !== null && value !== undefined){
                    if(value['status'] == 1){

                                    html=`<div class="col-xl-3 col-sm-6">
                                    <div class="card text-center">
                                        <div class="card-body">
                                            
                                            <div class="mx-auto mb-4">
                                                <img src="`+value['image']['url']+`" alt="" class="avatar-xl rounded-circle img-thumbnail">
                                            </div>
                                            <h5 class="font-size-16 mb-1"><a href="#" class="text-dark">`+value['nombre']+`</a></h5>
                                            <p class="text-muted mb-2">`+value['marca']+`</p>
                                            <p class="text-muted mb-2">`+value['modelo']+`</p>
                                            <p class="text-muted mb-2">`+value['precio']+`</p>`;
                                            if(value['status'] == 1){
                                                html +=`<p class="text-muted mb-2">Disponible</p>`;
                                            }else{
                                                html +=`<p class="text-muted mb-2">Vendido</p>`;
                                            }
                                        html += `</div>
    
                                        <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-outline-light text-truncate dollar-price" data-id="`+value['precio']+`"><a href="Javascript:void(0)"><i class="uil uil-user me-1"></i> Ver precio en dolares</a></button>
                                        </div>
                                    </div>
                                    <!-- end card -->
                                </div>`;

                        $('#vehicles-table').append(
                            html
                        ); 
                    }
                }
                
            }); 
        }else{

           $('#vehicles-table').append(`<div class="row">
           <div class="col-lg-12">
               <div class="card">
                   <div class="card-body">

                       <div class="row mt-5">
                           <div>
                               <div class="card">
                                   <div class="card-body overflow-hidden position-relative">
                                       <div>
                                           <i class="bx bx-help-circle widget-box-1-icon text-primary"></i>
                                       </div>
                                           <div class="faq-count">
                                           <h5 class="text-primary">:)</h5>
                                           </div>
                                       <h5 class="mt-3 aling-center">¿No puedes ver vehículos registrado?</h5>
                                       <p class="text-muted mt-3 mb-0 aling-center">Descuida esto no es un error.</p>
                                       <p class="text-muted mt-3 mb-0 aling-center">Pronto los podrás ver.</p>
                                   </div>
                                   <!-- end card body -->
                               </div>
                               <!-- end card -->
                           </div>
                           <!-- end col -->
                       </div>
                       <!-- end row -->
                   </div>
                   <!-- end  card body -->
               </div>
               <!-- end card -->
           </div>
           <!-- end col -->
       </div>
       <!-- end row -->`
); 
        }
        
    });
}