$( document ).ready(function() {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "8000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }

    $(function(){
        var token = localStorage.getItem("token");
        if(token != null && token != "" && token != undefined){
            window.location.href = base+"/admin-cars";
        }
        formValidateLogin();
    });
});

/////////////////////////////////////////   Out Document Ready    /////////////////////////////////////////////

var formValidateLogin = function(){
    $('#form-login').validate({
        rules: {
            email: {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                email: true,
            },
            password: {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
            }
        },
        messages: {
            email: {
                required:  'El email es obligatorio.',      
                email: 'Debe ser un email valido.'
            },
            password: {
                required: 'La contraseña es obligatoria.'
            }
        },
        
        highlight: function(element) {
            $(element).closest('.form-control').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-control').removeClass('has-error');
        },
        invalidHandler: function(form, validator) {
            if (!validator.numberOfInvalids())
                return;
        },
        errorElement: 'form-control',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            var email = $('#email').val();   
            var password = $('#password').val();   
            
            var settings = {
                "url": "http://localhost:3001/api/auth/login",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                  "email": email,
                  "password": password
                }),
              };
              
              $.ajax(settings).done(function (response) {
                if(response['success'] == 1){
                    localStorage.setItem("token", response['datos']['token']);
                    localStorage.setItem("email", response['datos']['user']['email']);
                    localStorage.setItem("name", response['datos']['user']['name']);
                    localStorage.setItem("role", response['datos']['user']['role'][0]);
                    localStorage.setItem("type", response['datos']['user']['type'][0]);
                    localStorage.setItem("id", response['datos']['user']['_id']);

                    window.location.href = base+"/admin-cars";

                    //var token = localStorage.getItem("token");
                    //var email = localStorage.getItem("email");
                    //var name = localStorage.getItem("name");
                    //var role = localStorage.getItem("role");
                    //var type = localStorage.getItem("type");
                    //var id = localStorage.getItem("id");
                }else{
                    toastr.error(response['message']);
                }
                
              });

        }
    });

};